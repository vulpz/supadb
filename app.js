/*|||||||||||||||||===SupaDB_Server===|||||||||||||||||||*/
/*_______________________________________________________*/


const 	express = require( 'express' )
		bodyParser = require( 'body-parser' )
	 	session = require( 'express-session' )
 		MongoStore = require( 'connect-mongo' )( session )
		cookieParser = require( 'cookie-parser' )

/*--------------===Inits and Declarations===-------------*/
/*_______________________________________________________*/

var app, db, ctrl, auth

var dir = __dirname

// Init Express app and return.
function initApp() {
	return new Promise( ( resolve, reject ) => {
		var app = express()
		if( app == null ) reject( 'Init::App_error' )
		resolve( app )
	} )
}

// Init Mongoose and return.
function initDB() {
	return new Promise( ( resolve, reject ) => {
		var mongo = require( './db/db.js' )
		if( mongo == null ) reject( 'Init::Mongo_error' )
		resolve( mongo )
	} )
}

// Init auth module and return.
function initAuth( exp ) {
	return new Promise( ( resolve, reject ) => {
		var auth = require( './auth/auth.js' )
		if( auth == null ) reject( 'Init::Auth_error' )
		resolve( auth )
	} )
}

// Init control module and return.
function initCtrl( exp, mon ) {
	return new Promise( ( resolve, reject ) => {
		var ctrl = require( './controllers/controller.js' )( exp, mon )
		if( ctrl == null ) reject( 'Init::Ctrl_error' )
		resolve( ctrl )
	} )
}

// Init 'app' and pass..
initApp()
	.then( ( app_p ) => {
		
		// Init 'mongoose' and pass..
		initDB()
			.then( ( db_p ) => {
				
				// Init 'auth' and pass..
				initAuth( app_p )
					.then( ( auth_p ) => {

						// Init 'ctrl' and pass..
						initCtrl( auth_p, db_p)
							.then( ( ctrl_p ) => {

								// Do everything!
								
								// Cookies!
								app_p.use( cookieParser() )

								// Setup session info.
								app_p.use( session( {
									store: new MongoStore(
										{ 
											mongooseConnection: db_p.connections[0],
										  	ttl: 60 * 60 * 24 * 2
										} ),
									cookie: 
										{ 
											path: '/',
											httpOnly: true,
											secure: false,
											maxAge: null
										},
									secret: 'nomoarCoff335#0r7463',
									resave: false,
									saveUninitialized: false
								} ) )

								// Init urlencoded parsing.
								var urlencodedParser = bodyParser.urlencoded( { extended: false, strict: false } )
								app_p.use( urlencodedParser )
								var jsonParser = bodyParser.json()

								// Allow 'public' file serving.
								app_p.use( express.static( 'public' ) )

								/*----------------===Handlers and Listeners===-----------*/
								/*_______________________________________________________*/


								// GET handler.
								app_p.get( '/', ctrl_p.home )

								// Login page handler.
								app_p.get( '/login', ctrl_p.login )

								// Login authentication handler.
								app_p.post( '/dologin', urlencodedParser, ctrl_p.doLogin )

								// Check session info.
								app_p.get( '/supadb', urlencodedParser, ctrl_p.doCheck,
									( req, res ) => { 
										res.sendFile( dir + '/public/html/supadb.html')
									} )

								// DB all students query.
								app_p.post( '/db_query_students_all', urlencodedParser, ctrl_p.dbQueryStudentsAll )

								// DB all staff query.
								app_p.post( '/db_query_staff_all', urlencodedParser, ctrl_p.dbQueryStaffAll )

								// DB add student.
								app_p.post( '/db_add_student', jsonParser, ctrl_p.dbAddStudent )

								// DB edit student.
								app_p.post( '/db_edit_student', jsonParser, ctrl_p.dbEditStudent )

								// DB delete student.
								app_p.post( '/db_del_student', jsonParser, ctrl_p.dbDeleteStudent )

								// DB add program.
								app_p.post( '/db_add_program', jsonParser, ctrl_p.dbAddProgram )

								// DB delete program.
								app_p.post( '/db_del_program', jsonParser, ctrl_p.dbDeleteProgram )

								// DB edit program.
								app_p.post( '/db_edit_program', jsonParser, ctrl_p.dbEditProgram )

								// DB add employment.
								app_p.post( '/db_add_employment', jsonParser, ctrl_p.dbAddEmployment )

								// DB edit employment.
								app_p.post( '/db_edit_employment', jsonParser, ctrl_p.dbEditEmployment )

								// DB delete employment.
								app_p.post( '/db_del_employment', jsonParser, ctrl_p.dbDeleteEmployment )

								/*------------------===Connections===--------------------*/
								/*_______________________________________________________*/


								// Turn on server and listen.
								const server = app_p.listen( 80, 'jpsmtools.umd.edu', () => {
									const host = server.address().address
									const port = server.address().port

									console.log( `Listening for ${host} on port ${port}...` )
								} )
							} )
					} )
			} )
	} )
