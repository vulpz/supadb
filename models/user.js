const mongoose = require( 'mongoose' )
const Schema = mongoose.Schema

// Create new schema for Users.
var UserSchema = new Schema( {
	username: String,
	password: String,
} )

// Export 'User' schema as a mongoose model; control.
module.exports = mongoose.model( 'User', UserSchema )
