const mongoose = require( 'mongoose' )
const extend = require( 'mongoose-schema-extend' )
const Schema = mongoose.Schema

// Import extendee and nestee.
var PersonSchema = mongoose.model( 'Person' ).schema,
	Program = require( '../models/program.js' ),
	Employment = require( '../models/employment.js' )

// Create new schema for students;
// extend person and program.
var StudentSchema = PersonSchema.extend( {
	residency: String,
	currentlyEmployed: Boolean,
	currentlyEnrolled: Boolean,
	programs: [ {
		type: Schema.Types.ObjectId,
		ref: 'Program'
	} ],
	employment: [ {
		type: Schema.Types.ObjectId,
		ref: 'Employment'
	} ]
}, { discriminatorKey: '_type' } )

module.exports = mongoose.model( 'Student', StudentSchema )
