const mongoose = require( 'mongoose' )
const Schema = mongoose.Schema

// Create new schema for Employment records.
var EmploymentSchema = new Schema( {
	_person: {
		type: Schema.Types.ObjectId,
		ref: 'Person'
	},
	position: String,
	name: String,
	sector: String,
	sDate: String,
	eDate: String
} )

module.exports = mongoose.model( 'Employment', EmploymentSchema )
