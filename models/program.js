const mongoose = require( 'mongoose' )
const Schema = mongoose.Schema

// Create new schema for Program records.
var ProgramSchema = new Schema( {
	_person: {
		type: Schema.Types.ObjectId,
		ref: 'Person'
	},
	name: String,
	concentration: String,
	sDate: String,
	eDate: String,
	complete: Boolean
} )

module.exports = mongoose.model( 'Program', ProgramSchema )
