const mongoose = require( 'mongoose' )
const Schema = mongoose.Schema

// Create new schema for person records.
var PersonSchema = new Schema( {
	fName: String,
	lName: String,
	phone: String,
	email: String,
	birthdate: String,
	UID: String
}, { collection: 'people', discriminatorKey: '_type' } )

// Export.
module.exports = mongoose.model( 'Person', PersonSchema )
