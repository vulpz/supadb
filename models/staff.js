const mongoose = require( 'mongoose' )
const extend = require( 'mongoose-schema-extend' )
const Schema = mongoose.Schema

// Import extendee and nestee.
var PersonSchema = mongoose.model( 'Person' ).schema

// Create new schema for staff;
// extend person.
var StaffSchema = PersonSchema.extend( {
	position: String,
	sDate: String,
	eDate: String
}, { discriminatorKey: '_type' } )

module.exports = mongoose.model( 'Staff', StaffSchema )