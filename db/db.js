const mongoose = require( 'mongoose' )

mongoose.Promise = global.Promise

// Connect to DB.
mongoose.connect( 'mongodb://supaapp:2448Fsysnsdr1337@localhost/jpsm' )
	.then( () => { console.log( 'DB connection successful...' ) } )
	.catch( err => { console.error( err ) } )

// Export mongoose module.
module.exports = mongoose