var mongo = require( 'mongoose' )
var Person = require( '../models/person.js' )
var Student = require( '../models/student.js' )
var Program = require( '../models/program.js' )

// Connect to DB.
mongo.connect( 'mongodb://localhost/test/jpsm' )
	.then( () => { console.log( 'DB connection successful...' ) } )
	.catch( err => { console.error( 'err?' ) } )

// var record = new Student( {
// 	fName: "Jane",
// 	lName: "Doe",
// 	email: "janed@student.net",
// 	phone: "8005551234",
// 	currentlyEnrolled: false,
// } )

// // Save document.
// record.save( err => {
// 	if( err ) console.log( err )
// 	console.log( 'record saved' )

// 	// Create new Program.
// 	var program = new Program( {
// 		_person: record._id,
// 		name: "MS",
// 		concentration: "Stat"
// 	} )

// 	// Save document.
// 	program.save( err => {
// 		if( err ) console.log( err )
// 		console.log( 'program saved' )

// 		// Push document into parent array.
// 		record.programs.push( program._id )
// 		console.log( 'program pushed to record' )

// 		// Save original doc again.
// 		record.save( err => {
// 			if( err ) console.log( err )
// 			console.log( 'resaved record' )
// 		})
// 	} )
// } )

Student
	.find( { } )
	.populate( 'programs' )
	.exec( ( err, students ) => {
		for( var student of students ) {
			console.log( student + '\n' )
		}
	} )
