const mongoose = require( 'mongoose' )
const Program = require( '../models/program.js' )
const Person = require( '../models/person.js' )
const Student = require( '../models/student.js' )

mongoose.Promise = global.Promise

// Connect to DB.
mongoose.connect( 'mongodb://localhost/test' )
	.then( () => {
		console.log( 'DB connection successful...' )
		Student
			.find( { _type: 'Student', fName: 'John' } )
			.populate( 'programs' )
			.exec( ( err, student ) => {
				Program
					.update(
						{ _id: '592bbc9488bc02229858d12f' },
						{
							name: 'MS'
						}, ( err, newVal, raw ) => {
						}
					 )
			} )
	} )
	.catch( err => { 
		//console.error( err )
	} )