/*|||||||||||||||||||||===NG_App===||||||||||||||||||||||*/
/*_______________________________________________________*/

var app = angular.module( 'SupaDBApp', [] )

	/*--------------------===Services===---------------------*/
	/*_______________________________________________________*/

	.factory( 'shr', [ '$rootScope', '$http', '$q', function( $rootScope, $http, $q ) {

		// Service objs.
		var headers = [],
			hide_students_rec = false,
			hide_staff_rec = true,
			hide_faculty_rec = true,
			hide_student = true,
			hide_staff = true,
			hide_faculty = true,
			hide_program = true,
			service = {}

//---------------------------------------------------------

		// Return array of headers.
		service.getHeaders = function() {
			return headers
		}

		// Set 'headers' and notify modal.
		service.setHeaders = function( data ) {
			headers = data
			$rootScope.$broadcast( 'headers:set' )
		}

		// Add programs to 'programs'; notify modal.
		service.addPrograms = function( data ) {

			let programs = {}
				programs.programs = []
				programs.student = data.id
				progs = data.programs
				
			for( let prog of progs ) { 

				let record = {}
					record = {
						name: prog.name,
						concentration: prog.concentration,
						start: prog.start,
						end: prog.end,
						complete: prog.complete,
						id: prog.id,
						student: data.id
					}

				programs.programs.push( record )
			}

			$rootScope.$broadcast( 'programs:set', programs )
		}

		service.addEmployment = function( data ) {

			let employ = {}
				employ.employment = []
				employ.student = data.id
				emps = data.employment

			for( let emp of emps ) {

				let record = {}
					record = {
						position: emp.position,
						name: emp.name,
						sector: emp.sector,
						start: emp.start,
						end: emp.end,
						id: emp.id,
						student: data.id
					}

				employ.employment.push( record )
			}

			$rootScope.$broadcast( 'employment:set', employ )
		}

		// Return array of notifications.
		service.getNotifications = function() {
			return notifications
		}

		// Add single notification to 'notifications'; notify modal.
		service.addNotification = function( noti ) {
			notifications.push( noti )
			$rootScope.$broadcast( 'notifications:set' )
		}

//---------------------------------------------------------			

		service.db_queryStudentsAll = function() {
			let studs = []
			$http.post( '/db_query_students_all' )
				.then( res => {

					var correctPrograms = function( programs ) {

						let progs = []

						for( let program of programs ) {

							let rec = {
								name: program.name,
								concentration: program.concentration,
								start: program.sDate,
								end: program.eDate,
								complete: program.complete,
								id: program._id
							}

							progs.push( rec )
						}

						return progs
					}

					var correctEmployment = function( employment ) {

						let employed = []

						for( let emp of employment ) {

							let rec = {
								position: emp.position,
								name: emp.name,
								sector: emp.sector,
								start: emp.sDate,
								end: emp.eDate,
								id: emp._id
							}

							employed.push( rec )
						}

						return employed
					}					

					for( let student of res.data.students ) {

						// Create a record (obj) from db response.
						let record = {}
						record = {
							lname: student.lName,
							fname: student.fName,
							email: student.email,
							phone: student.phone,
							residency: student.residency,
							enrolled: student.currentlyEnrolled.toString(),
							employed: student.currentlyEmployed.toString(),
							programs: correctPrograms( student.programs ),
							employment: correctEmployment( student.employment ),
							id: student._id
						}

						studs.push( record )
					}

					$rootScope.$broadcast( 'queryStudentsAll:do', studs )
				} )
		}

		service.db_queryStaffAll = function() {
			let staff = []
			$http.post( '/db_query_staff_all' )
				.then( res => {

					for( let member of res.data.staff ) {

						// Create a record (obj) from db response.
						let record = {}
						record = {
							lname: staff.lName,
							fname: staff.fName,
							email: staff.email,
							phone: staff.phone,
							position: staff.position,
							start: staff.sDate,
							end: staff.eDate,
							id: staff._id
						}

						staff.push( record )
					}

					$rootScope.$broadcast( 'queryStaffAll:do', staff )
				} )
		}

		service.db_addStudent = function( student ) {
			
			return $q( ( resolve, reject ) => {

				$http
					.post( '/db_add_student', {
						'lname':student.lname,
						'fname':student.fname,
						'email':student.email,
						'phone':student.phone,
						'residency':student.residency,
						'enrolled':student.enrolled,
						'employed':student.employed
					})
					.then( res => {
						resolve( res )
					} )
			} )
		}

		service.db_editStudent = function( student ) {

			return $q( ( resolve, reject ) => {

				$http
					.post( '/db_edit_student', {
						'id':student.id,
						'lname':student.lname,
						'fname':student.fname,
						'email':student.email,
						'phone':student.phone,
						'residency':student.residency,
						'enrolled':student.enrolled,
						'employed':student.employed
					} )
					.then( res => {
						resolve( 'OK' )
					} )
					.catch( err => {
						reject( 'Not OK' )
					} )
			} )
		}

		service.db_deleteStudent = function( student_id ) {

			return $q( ( resolve, reject ) => {

				console.log( 'delete: ' + student_id )
				$http
					.post( '/db_del_student', {
						'id': student_id
					} )
					.then( res => {
						resolve( 'OK' )
					} )
			} )

		}

		service.db_addProgram = function( program ) {

			return $q( ( resolve, reject ) => {

				$http
					.post( '/db_add_program', {
						'student': program.student,
						'name': program.name,
						'conc': program.concentration,
						'start': program.start,
						'end': program.end,
						'comp': program.comp
					} )
					.then( res => {
						resolve( 'OK' )
					} )
			} )
		}

		service.db_deleteProgram = function( data ) {

			return $q( ( resolve, reject ) => {
				
				$http
					.post( '/db_del_program', {
						'id': data.program,
						'student': data.student
					} )
					.then( res => {
						service.closeModal()
						service.queryStudentsAll
						resolve( 'OK' )
					} )
			} )
		}

		service.db_editProgram = function( program ) {

			return $q( ( resolve, reject ) => {
				
				$http
					.post( '/db_edit_program', {
						'id':program.id,
						'name':program.name,
						'concentration':program.conc,
						'start':program.start,
						'end':program.end,
						'completed':program.comp
					} )
					.then( res => {
						service.closeModal()
						service.db_queryStudentsAll
						resolve( 'OK' )
					} )
					.catch( err => {
						reject( 'Not OK' )
					} )
			} )
		}

		service.db_addEmployment = function( employment ) {

			return $q( ( resolve, reject ) => {

				$http
					.post( '/db_add_employment', {
						'student': employment.student,
						'position': employment.position,
						'name': employment.name,
						'sector': employment.sector,
						'start': employment.start,
						'end': employment.end
					} )
					.then( res => {
						resolve( 'OK' )
					} )
			} )
		}

		service.db_deleteEmployment = function( data ) {

			return $q( ( resolve, reject ) => {
				
				$http
					.post( '/db_del_employment', {
						'id': data.employment,
						'student': data.student
					} )
					.then( res => {
						service.closeModal()
						service.queryStudentsAll
						resolve( 'OK' )
					} )
			} )
		}

		service.db_editEmployment = function( employment ) {

			return $q( ( resolve, reject ) => {
				
				$http
					.post( '/db_edit_employment', {
						'id':employment.id,
						'name':employment.name,
						'concentration':employment.conc,
						'start':employment.start,
						'end':employment.end,
						'completed':employment.comp
					} )
					.then( res => {
						service.closeModal()
						service.db_queryStudentsAll
						resolve( 'OK' )
					} )
					.catch( err => {
						reject( 'Not OK' )
					} )
			} )
		}		

//---------------------------------------------------------

		service.getTableStudents = function() {
			return hide_students_rec
		}

		service.setTableStudents = function( data ) {
			hide_students_rec = data
			$rootScope.$broadcast( 'tablestudents:set' )
		}

		service.sortTable = function( data ) {
			$rootScope.$broadcast( 'sorttable:do', data )
		}

		service.getTableStaff = function() {
			return hide_staff_rec
		}

		service.setTableStaff = function( data ) {
			hide_staff_rec = data
			$rootScope.$broadcast( 'tablestaff:set' )
		}

		service.getTableFaculty = function() {
			return hide_faculty_rec
		}

		service.setTableFaculty = function( data ) {
			hide_faculty_rec = data
			$rootScope.$broadcast( 'tablefaculty:set' )
		}

//---------------------------------------------------------

		service.getStudetHide = function() {
			return hide_student
		}

		service.setStudentHide = function( data ) {
			hide_student = data
			$rootScope.$broadcast( 'hide_student:set', hide_student )
		}

		service.getStaffHide = function() {
			return hide_staff
		}

		service.setStaffHide = function( data ) {
			hide_staff = data
			$rootScope.$broadcast( 'hide_staff:set' )
		}

		service.getFacultyHide = function() {
			return hide_faculty
		}

		service.setFacultyHide = function( data ) {
			hide_faculty = data
			$rootScope.$broadcast( 'hide_faculty:set' )
		}

		service.getProgramHide = function() {
			return hide_program
		}

		service.setProgramHide = function( data ) {
			hide_program = data
			$rootScope.$broadcast( 'hide_program:set' )
		}

		// Notify modal to call 'openModal()'.
		service.openModal = function( data, callee ) {
			$rootScope.$broadcast( 'modal:open', [ data, callee ] )
		}

		// Notify modal to call 'closeModal()'
		service.closeModal = function( event ) {
			$rootScope.$broadcast( 'modal:close' )
		}

		// Return service.
		return service
	} ] )
