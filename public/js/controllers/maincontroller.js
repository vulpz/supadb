app.controller( 'MainController', [ '$scope', '$http', '$timeout', '$filter', '$q', 'shr',
	function( $scope, $http, $timeout, $filter, $q, shr ) {

		/**
		 *  Vars for students, programs, and methods.
		 */
		 $scope.init = function() {

			$scope.students = []
			$scope.staff = []
			$scope.faculty = []

			$scope.students_headers = [ 'Last name', 'First name',
					'Email', 'Phone', 'Residency', 'Currently enrolled',
					'Currently employed', 'Programs', 'Employment', 'Edit' ]
			$scope.staff_headers = [ 'Last name', 'First name',
					'Email', 'Phone', 'Position', 'Start date', 'End date', ' ' ]
			$scope.faculty_headers = []
			$scope.program_headers = [ 'Name', 'Concentration', 'Start date',
					'End date', 'Completed', 'Edit' ]
			$scope.employment_headers = [ 'Position', 'Employer', 'Sector',
					'Start date', 'Leave date' ]
					
			$scope.edit_student = false
			$scope.hide_student_table = true
			$scope.hide_staff_table = true

			shr.db_queryStudentsAll()
		 }
		 $scope.init()

		/**
		 *	Called when clicking 'close' or 'background' of modal;
		 *	will close the modal and reset scope 'programs' obj.
		 */
		$scope.closeProgramModal = shr.closeModal

		/**
		 *	Called to sort table; uses arg as switch for sort type.
		 */
		$scope.sortTable = function( data ) {

			// Save students, currently.
			var studentsOld = $scope.students

			// Filter array and update 'students'.
			$scope.students = $filter( 'orderBy' )( studentsOld, data )
			console.log( $scope.students )
		}

		/**
		 *	Edit student and replace in db.
		 */
		$scope.editStudent = function( $event, e ) {

			// Get row of record to edit.
			var button = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				count = row.childElementCount,
				del = row.children[ count - 1 ],
				sid = ''

				console.log(row.children)

			// Check for 'edit' mode.
			if( $scope.edit_student ) {

				sid = row.getAttribute( 'stud-id' )

				// Save, clean, and close.
				for( var j = 0; j < ( count - 2 ); j++ ) {

					let rec = row.children[ j ].children[ 0 ]
					rec.readOnly = true
					rec.classList.toggle( 'uneditable' )
				}

				shr.db_editStudent( e.student )
					.then( res => {
						button.classList.toggle( 'is-info')
						button.classList.toggle( 'is-warning')
						del.classList.toggle( 'hide-delete' )
						button.innerHTML = 'Edit'
						$scope.edit_student = false
						shr.db_queryStudentsAll()
					} )

			} else {

				// Loop through input in record and make editable.
				for( var i = 0; i < ( count - 2 ); i++ ) {

					let rec = row.children[ i ].children[ 0 ]
					rec.readOnly = false
					rec.classList.toggle( 'uneditable' )
				}

				button.classList.toggle( 'is-info')
				button.classList.toggle( 'is-warning')
				del.classList.toggle( 'hide-delete' )
				button.innerHTML = 'Save'
				$scope.edit_student = true
			}


		}

		/**
		 *	Called when clicking delete; calls modal for confirmation text.
		 */
		$scope.deleteStudent = function( $event, e ) {

			let record = $event.srcElement.parentElement.parentElement,
				record_id = record.getAttribute( 'stud-id' ),
				record_count = record.children.length,
				button = record.children[ record_count - 2 ].children[ 0 ],
				del = record.children[ 0 ].children[ 0 ]
			
			shr.db_deleteStudent( record_id )
				.then( res => {
					button.classList.toggle( 'is-info')
					button.classList.toggle( 'is-warning')
					del.classList.toggle( 'hide-delete' )
					button.innerHTML = 'Edit'
					$scope.edit_student = false
					shr.db_queryStudentsAll()
				} )
		}

		/**
		 *	Called when clicking a program 'Open' button;
		 *	will open a modal for viewing student programs.
		 */ 	
		$scope.openProgramModal = function( event ) {

			//console.log(event)
			// Store the 'clicked' student's object.
			let current_student = event.student

			// Call service to send student programs to modal.
			shr.addPrograms( current_student )

			// Push headers.
			// Open the modal.
			shr.openModal( $scope.program_headers, 'programs' )
		}

		/**
		 *	Called when clicking a employment 'Open' button;
		 *  will open a modal for viewing student employment.
		 */
		$scope.openEmploymentModal = function( event ) {

		 	// Store 'clicked' student object.
		 	let current_student = event.student

		 	// Call service to send student employment to modal.
		 	shr.addEmployment( current_student )

		 	// Push headers and open modal.
		 	shr.openModal( $scope.employment_headers, 'employment' )
		}

		

		/*------------===Watchers / Event handlers===------------*/
		/*_______________________________________________________*/

		$scope.$on( 'tablestudents:set', 
		() => {
			$scope.hide_student_table = shr.getTableStudents()
		} )

		$scope.$on( 'tablestaff:set',
		() => {
			$scope.hide_staff_table = shr.getTableStaff()
		} )

		$scope.$on( 'queryStudentsAll:do',
		( event, data ) => {
			$scope.students = data
		} )

		$scope.$on( 'queryStaffAll:do',
		( event, data ) => {
			$scope.staff = data
		} )

		$scope.$on( 'sorttable:do',
		( event, data ) => {
			$scope.sortTable( data )
		} )
} ] )
