/*|||||||||||||||||===ModalController===|||||||||||||||||*/
/*_______________________________________________________*/

app.controller( 'ModalController', [ '$scope', '$http', 'shr',
	function( $scope, $http, shr ) {

		// Get modal element for manipulation.
		var modal = document.getElementById( 'modal' )

		$scope.initScope = function() {

			// Scope objs.
			$scope.headers = []
			$scope.notifications = []
			$scope.programs = []
			$scope.employment = []
			$scope.student = ''
			$scope.add_student = false
			$scope.add_staff = false
			$scope.add_faculty = false
			$scope.view_program = false
			$scope.edit_program = false
			$scope.add_program = false
			$scope.view_employment = false
			$scope.edit_employment = false
			$scope.add_employment = false
			$scope.new_student = {
				lname: '',
				fname: '',
				email: '',
				phone: '',
				residency: '',
				enrolled: '',
				employed: ''
			}
			$scope.new_staff = {
				lname: '',
				fname: '',
				email: '',
				phone: '',
				position: ''
			}
			$scope.new_faculty = {
				lname: '',
				fname: '',
				email: '',
				phone: '',
				position: ''
			}
			$scope.new_program = {
				name: '',
				concentration: '',
				start: '',
				end: '',
				complete: ''
			}
			$scope.new_employment = {
				position: '',
				name: '',
				sector: '',
				start: '',
				end: ''
			}
		}

		$scope.initScope()

		// Called to open modal element.
		$scope.openModal = heads => {

			// Check for 'active' class.
			if( modal.classList.contains( 'is-active' ) ) {
				modal.classList.toggle( 'is-active' )
			}
			// Toggle 'active'.
			modal.classList.toggle( 'is-active' )
		}

		// Called to close modal element; clears scopes.
		$scope.closeModal = event => {

			if( modal.classList.contains( 'is-active' ) ) {
				modal.classList.toggle( 'is-active' )
			}
			$scope.initScope()
		}


		/**
		 *	Add student to db.
		 */
		$scope.addStudent = function() {
			
			shr.db_addStudent( $scope.new_student )
				.then( res => {
					shr.db_queryStudentsAll()
				})
			$scope.closeModal()
		}


		/**
		 *	Add student 'program'.
		 */
		$scope.addProgram = function( $event ) {

			// Gets row of record to add.
			var butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				count = row.childElementCount,
				pro = {
					id: '',
					name: '',
					conc: '',
					start: '',
					end: '',
					comp: '',
					student: ''
				}

			for( var k = 0; k < ( count - 1 ); k++ ) {
					
				// Loop through record (row) and child.
				let rec = row.children[ k ].children[ 0 ]

				// Save values to new object for db.
				switch( k ) {
					case 0: 
						pro.name = rec.value
						rec.value = ''
						break
					case 1:
						pro.concentration = rec.value
						rec.value = ''
						break
					case 2:
						pro.start = rec.value
						rec.value = ''
						break
					case 3:
						pro.end = rec.value
						rec.value = ''
						break
					case 4:
						pro.comp = rec.value
						rec.value = ''
						break
					default:
						break
				}
			}

			pro.student = $scope.student

			shr.db_addProgram( pro )
				.then( res => {
					$scope.closeModal()
					shr.db_queryStudentsAll()
				} )
		}


		/**
		 *	Delete student 'program'.
		 */
		$scope.deleteProgram = function( $event ) {
			let butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				program = row.getAttribute( 'prog-id' ),
				student = row.getAttribute( 'stud-id')

			shr.db_deleteProgram( {
				'program': program,
				'student': student } )
				.then( res => {
					$scope.closeModal()
					shr.db_queryStudentsAll()
				} )
		}


		/**
		 *	Edit student 'program'.
		 */
		$scope.editProgram = function( $event ) {

			//console.log($event)

			// Gets row of record to edit.
			var butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				count = row.childElementCount,
				del = row.children[ count - 1 ],
				pro = {
					id: ' ',
					name: ' ',
					conc: ' ',
					start: ' ',
					end: ' ',
					comp: ' '
				}

			// Check if in edit mode; if so..
			if( $scope.edit_program ) {

				pro.id = row.getAttribute( 'prog-id' )

				// Save, clean, and close.
				for( var j = 0; j < ( count - 1 ); j++ ) {
					
					// Loop through record (row) and make each child 'readonly'.
					let rec = row.children[ j ].children[ 0 ]
					rec.readOnly = true
					rec.classList.toggle( 'uneditable' )

					// Save values to new object for db.
					switch( j ) {
						case 0: 
							pro.name = rec.value
							break
						case 1:
							pro.conc = rec.value
							break
						case 2:
							pro.start = rec.value
							break
						case 3:
							pro.end = rec.value
							break
						case 4:
							pro.comp = rec.value
							break
						default:
							break
					}
				}

				// DB call to edit program based on object.
				shr.db_editProgram( pro )
					.then( res => {

						// Change buttons and classes back to hide UI.
						butt.innerHTML = 'Edit'
						del.classList.toggle( 'hide-delete' )
						$scope.edit_program = false
						shr.db_queryStudentsAll()
					} )
			} else {	// If not in edit mode...

				// Loop through input in record and make editable.
				for( var i = 0; i < ( count - 1 ); i++ ) {

					let rec = row.children[ i ].children[ 0 ]
					rec.readOnly = false
					rec.classList.toggle( 'uneditable' )
				}

				// Change buttons and classes to unhide UI.
				butt.innerHTML = 'Save'
				del.classList.toggle( 'hide-delete' )
				$scope.edit_program = true
			}
		}		

		/**
		 *	Add student 'employment'.
		 */
		$scope.addEmployment = function( $event ) {

			// Gets row of record to add.
			var butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				count = row.childElementCount,
				emp = {
					position: '',
					name: '',
					sector: '',
					start: '',
					end: ''
				}

			for( var k = 0; k < ( count - 1 ); k++ ) {
					
				// Loop through record (row) and child.
				let rec = row.children[ k ].children[ 0 ]

				// Save values to new object for db.
				switch( k ) {
					case 0: 
						emp.position = rec.value
						rec.value = ''
						break
					case 1:
						emp.name = rec.value
						rec.value = ''
						break
					case 2:
						emp.sector = rec.value
						rec.value = ''
						break
					case 3:
						emp.start = rec.value
						rec.value = ''
						break
					case 4:
						emp.end = rec.value
						rec.value = ''
						break
					default:
						break
				}
			}

			emp.student = $scope.student

			shr.db_addEmployment( emp )
				.then( res => {
					$scope.closeModal()
					shr.db_queryStudentsAll()
				} )
		}


		/**
		 *	Delete student 'employment'.
		 */
		$scope.deleteEmployment = function( $event ) {
			let butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				employment = row.getAttribute( 'emp-id' ),
				student = row.getAttribute( 'stud-id')

			shr.db_deleteEmployment( {
				'employment': employment,
				'student': student } )
				.then( res => {
					$scope.closeModal()
					shr.db_queryStudentsAll()
				} )
		}


		/**
		 *	Edit student 'employment'.
		 */
		$scope.editEmployment = function( $event ) {

			//console.log($event)

			// Gets row of record to edit.
			var butt = $event.srcElement,
				row = $event.srcElement.parentElement.parentElement,
				count = row.childElementCount,
				del = row.children[ count - 1 ],
				pro = {
					id: ' ',
					position: ' ',
					name: ' ',
					sector: ' ',
					start: ' ',
					end: ' '
				}

			// Check if in edit mode; if so..
			if( $scope.edit_employment ) {

				emp.id = row.getAttribute( 'emp-id' )

				// Save, clean, and close.
				for( var j = 0; j < ( count - 1 ); j++ ) {
					
					// Loop through record (row) and make each child 'readonly'.
					let rec = row.children[ j ].children[ 0 ]
					rec.readOnly = true
					rec.classList.toggle( 'uneditable' )

					// Save values to new object for db.
					switch( j ) {
						case 0: 
							emp.name = rec.value
							break
						case 1:
							emp.conc = rec.value
							break
						case 2:
							emp.start = rec.value
							break
						case 3:
							emp.end = rec.value
							break
						case 4:
							emp.comp = rec.value
							break
						default:
							break
					}
				}

				// DB call to edit program based on object.
				shr.db_editEmployment( emp )
					.then( res => {

						// Change buttons and classes back to hide UI.
						butt.innerHTML = 'Edit'
						del.classList.toggle( 'hide-delete' )
						$scope.edit_employment = false
						shr.db_queryStudentsAll()
					} )
			} else {	// If not in edit mode...

				// Loop through input in record and make editable.
				for( var i = 0; i < ( count - 1 ); i++ ) {

					let rec = row.children[ i ].children[ 0 ]
					rec.readOnly = false
					rec.classList.toggle( 'uneditable' )
				}

				// Change buttons and classes to unhide UI.
				butt.innerHTML = 'Save'
				del.classList.toggle( 'hide-delete' )
				$scope.edit_program = true
			}
		}		


		/*------------===Watchers / Event handlers===------------*/
		/*_______________________________________________________*/		

		// Called via 'shr' service; opens modal.
		$scope.$on( 'modal:open',
		( event, data ) => {
			switch( data[1] ) {
				case 'programs':
					$scope.view_program = true
					break
				case 'employment':
					$scope.view_employment = true
					break
				case 'student':
					$scope.add_student = true
					break
				case 'staff':
					$scope.add_staff = true
					break
				case 'faculty':
					$scope.add_faculty = true
					break
				default:
					break
			}

			$scope.headers = data[0]==null?[]:data[0]
			$scope.openModal()
		} )

		// Called via 'shr' service; closes modal.
		$scope.$on( 'modal:close',
		() => {
			$scope.closeModal()
			
		} )

		// Set hide var.
		$scope.$on( 'hide_student:set',
		( event, data ) => $scope.hide_student_add = data )

		$scope.$on( 'hide_staff:set',
		( event, data ) => $scope.hide_staff_add = data )

		$scope.$on( 'hide_faculty:set', 
		( event, data ) => $scope.hide_faculty_add = data )

		$scope.$on( 'hide_program:set',
		( event, data ) => $scope.hide_program_add = data )

		// Update headers when 'shr' props change.
		$scope.$on( 'headers:set',
		( event, data ) => {
			$scope.headers = shr.getHeaders()
		} )

		// Update programs when 'shr' props change.
		$scope.$on( 'programs:set',
		( event, data ) => {
			$scope.programs = data.programs
			$scope.student = data.student
		} )

		// Update employment when 'shr' props change.
		$scope.$on( 'employment:set',
		( event, data ) => {
			$scope.employment = data.employment
			$scope.student = data.student
		} )

		// Update notifications when 'shr' props change.
		$scope.$on( 'notifications:set',
		( event, data ) => {
			$scope.notifications = shr.getNotifications()
		} )
} ] )