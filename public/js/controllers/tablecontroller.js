app.controller( 'TableController', [ '$scope', '$http', '$sce', 'shr',
	function( $scope, $http, $sce, shr ) {
	

		// Holds table state; for choosing which db to operate on.
		$scope.states = [ 'Students', 'Staff', 'Faculty' ]
		$scope.state = ''
		// Holds table query.
		$scope.query = 'all'
		$scope.filter = ''
		$scope.filters = [
			{ 'text': $sce.trustAsHtml('Last name &uarr;'), 'val': 'lname' },
			{ 'text': $sce.trustAsHtml('Last name &darr;'), 'val': '-lname' },
			{ 'text': $sce.trustAsHtml('First name &uarr;'), 'val': 'fname' },
			{ 'text': $sce.trustAsHtml('First name &darr;'), 'val': '-fname' },
			{ 'text': $sce.trustAsHtml('Residency &uarr;'), 'val': 'residency' },
			{ 'text': $sce.trustAsHtml('Residency &darr;'), 'val': '-residency' }
		]

		// Called to add a student to the db.
		$scope.addStudent = function( event ) {
			shr.openModal( null, 'student' )
		}

		$scope.$watch( 'filter',
		( newVal, oldVal ) => {
			if( newVal.length > 0 ) {
				console.log( newVal + 'selected..' )
				shr.sortTable( newVal )
			}
		} )
		
		$scope.$watch( 'state',
		( newVal, oldVal ) => {
			if( newVal.length > 0 ) {
				switch( newVal ) {
					case 'Students':
						shr.setTableStaff( true )
						shr.setTableFaculty( true )
						shr.setTableStudents( false )
						break
					case 'Staff':
						shr.setTableStudents( true )
						shr.setTableFaculty( true )
						shr.setTableStaff( false )
						break
					case 'Faculty':
						shr.setTableStudents( true )
						shr.setTableStaff( true )
						shr.setTableFaculty( false )
						break
					default:
						break
				}
			}
		} )
} ] )
