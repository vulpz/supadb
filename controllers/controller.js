// Create local dir path string.
const dir = __dirname.substr( 0, ( __dirname.length - 11 ) )
const Person = require( '../models/person.js' )
const Program = require( '../models/program.js' )
const Employment = require( '../models/employment.js' )
const Student = require( '../models/student.js' )
const Staff = require( '../models/staff.js' )
const fs = require( 'fs' )
const stringify = require( 'json-stringify-safe' )

// Init controller object for exporting.
var controller = {}

// Home handler; produces home (index) page.
controller.home = ( req, res, next ) => {
	return res.sendFile( dir + '/public/html/index.html' )
}

// Login handler; produces login page.
controller.login = ( req, res, next ) => {
	return res.sendFile( dir + '/public/html/login.html' )
}

// Login POST; authenticate.
controller.doLogin = ( req, res, next ) => {

	// authenticate by passing POST username and password to auth check.
	console.log( 'Login attempt using user ' + req.body.username )

	controller.auth
		.findUser( 
			req.body.username,
			req.body.password, 
			( err, user ) => {
				// if 'err' is not null..
				if( err ) return next( res.send( err ) )
				// if 'err' is null and user is null..
				if( !user ) return next( res.redirect( '/login' ) )
				// Hey, congratz! You made it through; have a coffee.
				req.session.user = user._id
				return next( res.redirect( '/' ), console.log( 'Logged in as ' + user.username ) )
			}
		)
}

controller.doCheck = ( req, res, next ) => {

	// If user session data, check passed.
	if( req.session.user == null ) return res.redirect( '/login' )
	return next()
}

controller.dbQueryStudentsAll = ( req, res, next ) => {

	// Use 'Student' schema to find 'all' students.
	Student
		.find( { _type: 'Student' } )	// Find.
		.populate( 'programs' )			// Populate.
		.populate( 'employment' )
		.exec( ( err, students ) => {	// Return.
			return res.json( { students } )
		} )
}

controller.dbQueryStaffAll = ( req, res, next ) => {

	// Use 'Staff' schema to find 'all' staff.
	Staff
		.find( { _type: 'Staff' } )
		.exec( ( err, staff ) => {
			return res.json( { staff } )
		} )
}

controller.dbAddStudent = ( req, res, next ) => {

	var student = new Student( {
		lName: req.body.lname,
		fName: req.body.fname,
		email: req.body.email,
		phone: req.body.phone,
		residency: req.body.residency,
		currentlyEnrolled: ( req.body.enrolled === 'true' ),
		currentlyEmployed: ( req.body.employed === 'true' )
	} )

	student.save( ( err, r ) => {
		res.send( 'OK' )
	} )
	
}

controller.dbEditStudent = ( req, res, next ) => {

	Student
		.update( 
			{ _id: req.body.id },

			{
				lName: req.body.lname,
				fName: req.body.fname,
				email: req.body.email,
				phone: req.body.phone,
				residency: req.body.residency,
				currentlyEnrolled: req.body.enrolled,
				currentlyEmployed: req.body.employed
			}, ( err, newVal, oldVal ) => {
				if( !err ) res.send( 'OK' )
			} )
}

controller.dbDeleteStudent = ( req, res, next ) => {

	Student
		.remove( { _id: req.body.id },
		err => {
			if( !err ) res.send( 'OK' )
		} )
}

controller.dbAddProgram = ( req, res, next ) => {

	var program = new Program( {
		_person: req.body.student,
		name: req.body.name,
		concentration: req.body.conc,
		sDate: req.body.start,
		eDate: req.body.end,
		complete: ( req.body.comp === 'true' )
	} )

	program.save( ( err, r ) => {

		Student
			.find( { _id: r._person } )
			.exec( ( err, stds ) => {
				if( err ) console.log( 'BROKEDED' )
				stds[0].programs.push( r._id )
				stds[0].save( err => {
					if( !err ) res.send( 'OK' )
				} )
			} )
	} )
}

controller.dbEditProgram = ( req, res, next ) => {


	Program
		.update(
			{ _id: req.body.id },
			
			{
				name: req.body.name,
				concentration: req.body.concentration,
				sDate: req.body.start,
				eDate: req.body.end,
				completed: req.body.completed
			}, ( err, newVal, raw ) => {

				// Response.
				res.send( 'OK' )
			} )
}

controller.dbDeleteProgram = ( req, res, next ) => {


	// Student model.
	Student
		.find( { _id: req.body.student } )		// Find student by id.
		.populate( 'programs' )					// Populate the programs array.
		.exec( ( err, r ) => {

			if( !err ) {
				var toremove = r[0].programs.map( prop => {
					return prop._id
				} ).indexOf( req.body.id )

				r[0].programs.splice( toremove, 1 )
				r[0].save( err => {
				
					Program
						.remove( { _id: req.body.id }, err => {
							res.send( 'OK' )
						} )
				} )
			}
		} )
}

controller.dbAddEmployment = ( req, res, next ) => {

	var employment = new Employment( {
		_person: req.body.student,
		position: req.body.position,
		name: req.body.name,
		sector: req.body.sector,
		sDate: req.body.start,
		eDate: req.body.end
	} )

	employment.save( ( err, r ) => {

		Student
			.find( { _id: r._person } )
			.exec( ( err, stds ) => {
				if( err ) console.log( 'BROKEDED' )
				stds[0].employment.push( r._id )
				stds[0].save( err => {
					if( !err ) res.send( 'OK' )
				} )
			} )
	} )
}

controller.dbEditEmployment = ( req, res, next ) => {


	Employment
		.update(
			{ _id: req.body.id },
			
			{
				position: req.body.position,
				name: req.body.name,
				sector: req.body.sector,
				start: req.body.start,
				end: req.body.end
			}, ( err, newVal, raw ) => {

				// Response.
				res.send( 'OK' )
			} )
}

controller.dbDeleteEmployment = ( req, res, next ) => {

	// Student model.
	Student
		.find( { _id: req.body.student } )		// Find student by id.
		.populate( 'employment' )				// Populate the employment array.
		.exec( ( err, r ) => {
			if( !err ) {
				var toremove = r[0].employment.map( prop => {
					return prop._id
				} ).indexOf( req.body.id )

				r[0].employment.splice( toremove, 1 )
				r[0].save( err => {
				
					Employment
						.remove( { _id: req.body.id }, err => {
							res.send( 'OK' )
						} )
				} )
			}
		} )
}

// Export module; take an 'auth' instance to init.
module.exports = ( authentication ) => {
	controller.auth = authentication
	return controller
}
