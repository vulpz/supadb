const User = require( '../models/user.js' )

// 'auth' object for exporting.
var auth = {}

// Add 'User' model to 'auth' object; for exporting.
auth.User = User

// Find a user using mongoose model; takes creds and tests.
auth.findUser = ( username, password, callback ) => {

	// Test username..
	User.findOne( { username: username }, ( err, usr ) => {
		// if error..
		if( err ) return callback( err )
		// if user not found..
		if( !usr ) return callback( null, false )
		// if password does not match..
		if( password != usr.password ) return callback( null, false )
		// Yay! Have a coffee.
		return callback( null, usr )
	} )
}

// Export 'auth' object; take Express app instance.
module.exports = auth
